import java.util.*;
import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.core.sockjs.SockJSServer;
import org.vertx.java.deploy.Verticle;
import org.vertx.java.core.eventbus.EventBus;


/**
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public class server2 extends Verticle {
	
	//Declarar variaveis globais.
	
	public void start() throws Exception {
			
		JsonArray arrayinbound = new JsonArray("[{}]");
		JsonArray arrayoutbound = new JsonArray("[{}]");
		//configuracao da variavel para modulo webserver
		JsonObject configWebServer = new JsonObject();
		configWebServer.putString("host", "193.136.92.191");
		configWebServer.putNumber("port", 8080);
		configWebServer.putArray("inbound_permitted", arrayinbound);
		configWebServer.putArray("outbound_permitted", arrayoutbound);
		configWebServer.putBoolean("bridge", true);

		container.deployModule("vertx.web-server-v1.0", configWebServer);
	}
}
